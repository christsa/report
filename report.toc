\contentsline {chapter}{Preface}{v}{chapter*.6}
\contentsline {chapter}{Abstract}{vii}{chapter*.7}
\contentsline {chapter}{Symbols}{ix}{chapter*.8}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Review of Literature}{3}{chapter.2}
\contentsline {chapter}{\numberline {3}Methodology}{5}{chapter.3}
\contentsline {subsection}{\numberline {3.0.1}Elliptic Fourier Descriptors}{5}{subsection.3.0.1}
\contentsline {chapter}{\numberline {4}Experiments and Results}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Simulation}{7}{section.4.1}
\contentsline {section}{\numberline {4.2}Real Robot}{7}{section.4.2}
\contentsline {chapter}{\numberline {5}Discussion and Future Work}{9}{chapter.5}
\contentsline {chapter}{\numberline {6}Conclusion}{11}{chapter.6}
\contentsline {chapter}{Bibliography}{13}{chapter*.12}
\contentsline {chapter}{\numberline {A}Irgendwas}{15}{appendix.A}
\contentsline {chapter}{\numberline {B}Datasheets}{17}{appendix.B}
